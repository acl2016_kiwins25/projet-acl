import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TirTest {

	private Tir tir=null; 	

	@Before
	public void setUp() throws Exception {
		tir = new Tir();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testSetX() {
		tir.setX(120);
		assertEquals("Le X est incorrect", 120 ,tir.getX());
	}
	@Test
	public void testSetY() {
		tir.setY(80);
		assertEquals("Le Y est incorrect", 80 ,tir.getY());
	}
}