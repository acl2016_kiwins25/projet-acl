import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class ExplosionTest {

	public Explosion expl = null;

	@Before
	public void setUp() throws Exception {
		expl = new Explosion(0,0);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAjoutExplosion() {
		assertNotNull(expl);
	}
	@Test
	public void testSetX() {
		expl.setX(0);
		assertEquals(0,expl.getX());
	}

}
