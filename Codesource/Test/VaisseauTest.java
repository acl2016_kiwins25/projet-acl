import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class VaisseauTest {

	public Vaisseau vaisseau = null;

	@Before
	public void setUp() throws Exception {
		vaisseau = new Vaisseau();
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAjoutVaisseau() {
		assertNotNull(vaisseau);
	}
	public void testSetX() {
		vaisseau.setX(20);
		assertEquals(20,vaisseau.getX());
	}

}
