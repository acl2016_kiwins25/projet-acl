

import static org.junit.Assert.*;

import java.io.IOException;
import org.junit.Before;
import org.junit.Test;


public class AlienTest  {

	public Alien alien = null;
	int alea=0;


	@Before
	public void setUp() throws Exception {
		alien = new Alien();
		alea = alien.GenerationAlea();
	}

	/*
	@After
	public void tearDown() throws Exception {
		super.tearDown();
		alien = null;
	}*/

	
	@Test
	public void testAjoutAlien() throws IOException{
		assertNotNull(alien);
	}
	@Test
	public void GenerationAleaTest() throws IOException{
		assertNotNull(alea);
		assertTrue(alea<=760);
		assertTrue(alea>=10);
	}
	@Test
	public void collisionShotTest(){
		assertFalse(alien.collisionShot(800, 0));
		
	}
	@Test
	public void Testdeplacement(){
		int a = alien.getY();
		alien.deplacement();
		int b = alien.getY();
		assertEquals(a,b);
	}

}
