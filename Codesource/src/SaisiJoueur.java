package SpaceInvaders;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class SaisiJoueur extends JFrame implements ActionListener{

	
		private JTextField textField;
		private JLabel label;
		private JButton button;
		private int scoreJoueur;
		private JPanel fenetre;
		
		public SaisiJoueur(int scoreJoueur){
			super();
			this.scoreJoueur=scoreJoueur;
			build();//On initialise notre fen�tre
			System.out.println(this.scoreJoueur);
		}
		
		private void build(){
			setTitle("Nom Joueur"); //On donne un titre � l'application
			setSize(250,200); //On donne une taille � notre fen�tre
			setLocationRelativeTo(null); //On centre la fen�tre sur l'�cran
			setResizable(false); //On emp�che le redimensionnement
			setAlwaysOnTop(true);
			setContentPane(buildContentPane());
			setUndecorated(true);
		}
		
		private JPanel buildContentPane(){
	
			final JPanel panel = new JPanel(){
					private int compteur=0;

					protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				// affichage du mod�le du jeu
				if(this.compteur>0)
				{
					affichage(g);
					
				}
				compteur++;
			}};
			
			panel.setLayout(new FlowLayout());
			
			
			textField = new JTextField();
			textField.setColumns(10);
			
			panel.add(textField);
			
			label = new JLabel("Votre nom");
			
			panel.add(label);

			JButton bouton = new JButton(new GetAction(this, "Enregistrer", scoreJoueur));
			
			panel.add(bouton);
			
			this.button=bouton;
			
			JButton fermeture = new JButton("Fermer");
			fermeture.addActionListener(this);
			
			panel.add(fermeture);
			
			this.fenetre=panel;
			
			return this.fenetre;
		}
		
		public void affichage(Graphics g){
			this.button.setEnabled(false);
			for(int i=0;i<5;i++){
				g.setColor(Color.BLACK);
				g.setFont(new Font("Monaco", Font.PLAIN, 20));
				g.drawString(LectureFichier.nomsJoueurs.get(i)+" / "+LectureFichier.scoresJoueurs.get(i), 40, 90+i*25);
			}
			
			this.fenetre.repaint();
		}
		
		public JTextField getTextField(){
			return textField;
		}
		
		public JLabel getLabel(){
			return label;
		}

		@Override
		public void actionPerformed(ActionEvent arg0) {
			
			this.dispose();
						
		}
		
		

}
