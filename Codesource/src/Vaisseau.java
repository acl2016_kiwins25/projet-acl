package SpaceInvaders;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

public class Vaisseau extends ModeleJeu{
	
	private String image_vaisseau_joueur="Vaisseau.png";
	private Image vaisseauJoueur;
	private int x;
	private final int y;
	
	
	public Vaisseau(){
		try {
			this.vaisseauJoueur= ImageIO.read(new File(image_vaisseau_joueur));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.x=376;
		this.y=550;
	}
	
	protected void calcul(){
			
	}
	
	protected void affichage(Graphics g){
		
		g.drawImage(this.vaisseauJoueur, this.x, this.y, 50, 58, null);
		
	}
	
	
	

	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected void setX(int x) {
		this.x = x;
	}

	

	
	
	
	
}
