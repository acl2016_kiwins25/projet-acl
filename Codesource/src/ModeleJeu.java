package SpaceInvaders;
import java.awt.Graphics;

import javax.swing.JPanel;

abstract class ModeleJeu {
	
	
	abstract void calcul();
	
	abstract void affichage(Graphics g);

}
