package SpaceInvaders;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JPanel;

public class MoteurJeu extends ModeleJeu {

	private Vaisseau vaisseau;
	private Etoile etoile;
	private GameOver gameOver = new GameOver();
	private Variable variables;
	
	// Tableaux dans lesquels seront stock�s les dif�rentes entit�s presentes dans le jeu
	ArrayList<Tir> tabTir = new ArrayList<Tir>();
	private ArrayList<Alien> tabAlien = new ArrayList<Alien>();
	private ArrayList<Explosion> tabExplosion = new ArrayList<Explosion>();
	
	// Temporisations
	private int tempoapp;
	private int tempocalcul;
	private int tempoNiveau;
	private int tempoAffichage = 0; 
	private int tempoTir = 0;

	
	boolean droitTir;
	private boolean pause = false;
	
	// Variables relatifs au niveaux 
	private int scoreNiveau;
	private int affNiveau;

	//tableau des touches pressees : fleche haut, fleche gauche, fleche droite, entrer, espace
	private  boolean [] tabTouches ={false,false,false,false,false};


	protected MoteurJeu(){
		this.vaisseau=new Vaisseau();
		this.variables=new Variable();
		this.etoile=new Etoile();

	}

	protected void calcul() {
		if(variables.getVieJoueur()>0 && pause==false){
			// on teste s'il existe une collision alien/shot
			for (int i = 0; i < tabTir.size(); i++) {

				for (int j = 0; j < tabAlien.size(); j++) {

					if(tabAlien.get(j).collisionShot(tabTir.get(i).getX(), tabTir.get(i).getY())==true){
						Explosion explosion = new Explosion(tabAlien.get(j).getX(), tabAlien.get(j).getY());
						tabExplosion.add(explosion);
						tabAlien.get(j).setVieAlien(tabAlien.get(j).getVieAlien()-1);
						if(tabAlien.get(j).getVieAlien()==0) {
							tabAlien.remove(j);
						}
						tabTir.remove(i);
						variables.setScore(variables.getScore()+1);
						scoreNiveau++;
						passageNiveau();
						break;
					}
				}
			}


			// si le tir a ete effectue par le joueur
			for (int i = 0; i < tabTir.size(); i++) {

				tabTir.get(i).calcul(); 

				// on teste si le tir est toujours dans la fenetre
				if (tabTir.get(i).getY()<0){
					tabTir.remove(i);
				}
			}


			tempocalcul++;
			tempoapp++;

			for (int i = 0; i < tabExplosion.size(); i++) {
				tabExplosion.get(i).setTemporisation(tabExplosion.get(i).getTemporisation()+1);
				if (tabExplosion.get(i).getTemporisation() >= 10) {
					tabExplosion.get(i).setTemporisation(0);
					tabExplosion.remove(i);
				}

			}

			if (tempocalcul >= variables.getVitesseAlien()){
				tempocalcul = 0;

				// On determine le deplacement des aliens 
				for (int i = 0; i < tabAlien.size(); i++) {

					tabAlien.get(i).calcul();  
					// On cree une explosion quand l'alien arrive au niveau du vaisseau 
					if (tabAlien.get(i).getY()>550){
						Explosion explosion = new Explosion(tabAlien.get(i).getX(), 550);
						tabExplosion.add(explosion);
						tabAlien.remove(i);
						// On implemente le score a chaque explosion
						variables.setVieJoueur(variables.getVieJoueur()-1);
					}
				}
			}
			// Tout les 20 tics on fait apparaitre un alien
			if (tempoapp >= variables.getApparitionAlien()){
				tempoapp = 0;
				Alien newalien = new Alien(variables.getVieAlien());
				tabAlien.add(newalien);
			}
			tempoTir++;
			// On autorise le tir
			if (tempoTir >= 20) {
				droitTir = true;
				tempoTir = 0;
			}
		}

	}



	protected void affichage(Graphics g) {

		// activer l'anti-aliasing du dessin
		Graphics2D g2 = (Graphics2D) g;
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		if(variables.getVieJoueur()>0) {
			if(pause==false) {
				etoile.affichage(g);
				vaisseau.affichage(g);

				// si le tir a ete effectue par le joueur
				for (int i = 0; i < tabTir.size(); i++) {
					tabTir.get(i).affichage(g);
				}
				// On affiche tous les aliens
				for (int i = 0; i < tabAlien.size(); i++) {
					tabAlien.get(i).affichage(g); 
				}
				// On affiche les explosions
				for (int i = 0; i < tabExplosion.size(); i++) {
					tabExplosion.get(i).affichage(g);  
				}
				//On affiche le score
				variables.affichageScore(g);

				if(affNiveau>=0 && affNiveau<3) {
					tempoNiveau++;
					affichageNiveau(variables.getNiveau(), g);
					if(tempoNiveau==30) {
						tempoNiveau = 0;
						affNiveau++;
					}
				}
			}

			//affichage du menu pause
			if(pause==true) {
				g.setColor(Color.BLACK);
				g.setFont(new Font("Monaco", Font.PLAIN, 60));
				g.drawString("Pause", 320, 300);
				g.setColor(Color.RED);
				g.setFont(new Font("Monaco", Font.PLAIN, 20));
				g.drawString("Pour reprendre la partie en cours, appuyez sur espace", 165, 380);
				g.drawString("Pour recommencer, appuyez sur entrer", 230, 420);
			}

		}



		else {
			//Affichage de l'interface de game over
			tempoAffichage++;
			gameOver.affichage(g);
			gameOver.affichageGameOver(g,tempoAffichage,variables.getScore());
			scoreNiveau = 0;
			if(variables.getAffichageMeilleursScores()==true)
			{
				variables.setAffichageMeilleursScores(false);
				SaisiJoueur tableauScores=new SaisiJoueur(variables.getScore());
				tableauScores.setVisible(true);
			}
			if(tempoAffichage>=50) {
				tempoAffichage = 0;
			}
		}
	}






	protected void touchePresse(KeyEvent event) {	

		//mise � jour des variables pressees
		if(event.getKeyCode()==KeyEvent.VK_UP) tabTouches[0] = true;
		if(event.getKeyCode()==KeyEvent.VK_LEFT) tabTouches[1] = true;
		if(event.getKeyCode()==KeyEvent.VK_RIGHT) tabTouches[2] = true;
		if(event.getKeyCode()==KeyEvent.VK_ENTER) tabTouches[3] = true;
		if(event.getKeyCode()==KeyEvent.VK_SPACE) tabTouches[4] = true;

	}

	protected void gestionDuClavier() {	


		int px = this.vaisseau.getX();

		// deplacement a droite
		if(tabTouches[2]==true && tabTouches[0]==false && tabTouches[1]==false){
			if(px<756)
			{
				this.vaisseau.setX(px+10);
			}
		}

		//deplacement a gauche
		else if(tabTouches[1]==true && tabTouches[0]==false && tabTouches[2]==false){
			if(px>10)
			{
				this.vaisseau.setX(px-10);
			}
		}

		//tir joueur
		else if(tabTouches[0]==true && tabTouches[2]==false && tabTouches[1]==false){
			if(droitTir==true){
				Tir newtir = new Tir();
				tabTir.add(newtir);
				newtir.setX(this.vaisseau.getX()+12);
				newtir.setY(this.vaisseau.getY()-29);
				droitTir = false;
				tempoTir=0;
			}
		}

		//deplacement gauche et tir
		else if(tabTouches[1]==true && tabTouches[0]==true && tabTouches[2]==false){
			if(droitTir==true){
				Tir newtir = new Tir();
				tabTir.add(newtir);
				newtir.setX(this.vaisseau.getX()+12);
				newtir.setY(this.vaisseau.getY()-29);
				droitTir = false;
				tempoTir=0;
			}
			if(px>10)
			{
				this.vaisseau.setX(px-10);
			}
		}

		//deplacement droite et tir
		else if(tabTouches[2]==true && tabTouches[0]==true && tabTouches[1]==false){
			if(droitTir==true){
				Tir newtir = new Tir();
				tabTir.add(newtir);
				newtir.setX(this.vaisseau.getX()+12);
				newtir.setY(this.vaisseau.getY()-29);
				droitTir = false;
				tempoTir=0;
			}
			if(px<756)
			{
				this.vaisseau.setX(px+10);
			}
		}

		//Touche pour recommencer une partie
		else if(tabTouches[3]==true && variables.getVieJoueur()<=0) {
			tabExplosion.clear();
			tabAlien.clear();
			tabTir.clear();
			variables.InitialisationJeu(vaisseau);
		}

		//Touche pour activer la pause
		else if(tabTouches[4]==true && variables.getVieJoueur()>0 && pause==false) {
			pause = true;
		}

		//Touche pour reprendre la partie
		else if (tabTouches[4]==true && variables.getVieJoueur()>0 && pause==true) {
			pause = false;
		}

		//Touche pour recommencer la partie depuis le menu de pause
		else if (tabTouches[3]==true && variables.getVieJoueur()>0 && pause==true) {
			tabExplosion.clear();
			tabAlien.clear();
			tabTir.clear();
			pause = false;
			variables.InitialisationJeu(vaisseau);
		}
		else;

	}

	protected void toucheRelache(KeyEvent event) {	

		//mise � jour des variables relachees
		if(event.getKeyCode()==KeyEvent.VK_UP) tabTouches[0] = false;
		if(event.getKeyCode()==KeyEvent.VK_LEFT) tabTouches[1] = false;
		if(event.getKeyCode()==KeyEvent.VK_RIGHT) tabTouches[2] = false;
		if(event.getKeyCode()==KeyEvent.VK_ENTER) tabTouches[3] = false;
		if(event.getKeyCode()==KeyEvent.VK_SPACE) tabTouches[4] = false;
	}

	//Methode permettant de calculer le passage de niveau
	protected void passageNiveau() {
		// On passe le niveau tout les 10*niveau aliens tu�s
		if(scoreNiveau==10*variables.getNiveau()) {
			variables.setNiveau(variables.getNiveau()+1);
			if(variables.getNiveau()%2==0 && variables.getVitesseAlien()>2) {
				// La vitesse des aliens augmente
				variables.setVitesseAlien(variables.getVitesseAlien()-2);
			}
			if(variables.getNiveau()%2!=0 && variables.getApparitionAlien()>2) {
				// Les aliens apparaisent plus vite
				variables.setApparitionAlien(variables.getApparitionAlien()-2);
			}
			if(variables.getNiveau()%3==0) {
				// Le joueur gagne une vie
				variables.setVieJoueur(variables.getVieJoueur()+1);
			}
			if(variables.getNiveau()%5==0) {
				// Les aliens gagnent une vie
				variables.setVieAlien(variables.getVieAlien()+1);
			}
			scoreNiveau=0;
			affNiveau=0;
		}
	}

	//Methode permettant d'afficher le passage de niveau
	private void affichageNiveau(int niveau, Graphics g) {
		g.setColor(Color.WHITE);
		g.setFont(new Font("Monaco", Font.PLAIN, 60));
		//Pour faire clignoter
		if ((tempoNiveau<=30)&&(tempoNiveau>=15)){
			g.drawString("Niveau "+variables.getNiveau(), 285, 200);
		}
	}


}
