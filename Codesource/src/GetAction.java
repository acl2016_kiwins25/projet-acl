package SpaceInvaders;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.AbstractAction;
import javax.swing.Icon;

public class GetAction extends AbstractAction {

	private SaisiJoueur fenetre;
	private int scoreJoueur;

	public GetAction(SaisiJoueur fenetre, String texte, int scoreJoueur){
		super(texte);
		this.scoreJoueur=scoreJoueur;
		this.fenetre = fenetre;
	}

	public void actionPerformed(ActionEvent e) { 
		String texteUtilisateur = fenetre.getTextField().getText();
		try {
			LectureFichier.lectureLignes();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LectureFichier.compareScore(texteUtilisateur,this.scoreJoueur);
		
		try {
			LectureFichier.ecritureLignes();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
			
		fenetre.affichage(fenetre.getGraphics());
		
		
	}


}


