package SpaceInvaders;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

public class Tir extends ModeleJeu{
	private String image_bullet="Green_laser.png";
	private Image tir;
	private int y;
	private int x;
	
	
	protected Tir(){
		
		try {
			this.tir= ImageIO.read(new File(image_bullet));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
		
	
	protected void calcul(){
		deplacement();
		
	}
	
	protected void affichage(Graphics g){
		
		JLabel tir=new JLabel();
		g.drawImage(this.tir, this.x, this.y, 18, 39, null);
		
	}
	
	
	protected void deplacement(){
		
		this.y=this.y-10;		
	}
	

	protected int getX() {
		return x;
	}


	protected void setX(int x) {
		this.x = x;
	}


	protected void setY(int y) {
		this.y = y;
	}


	protected int getY() {
		return y;
	}
	
	
	
}
