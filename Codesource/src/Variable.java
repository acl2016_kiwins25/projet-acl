package SpaceInvaders;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class Variable {
	
	private int niveau = 1;
	private int tempoAlien;
	private int vieJoueur = 3;
	private int vieAlien = 1;
	private int score = 0;
	private int vitesseAlien = 40;
	private int apparitionAlien = 60;
	private Boolean affichageMeilleursScores=true;
	
	public Boolean getAffichageMeilleursScores() {
		return affichageMeilleursScores;
	}

	public void setAffichageMeilleursScores(Boolean affichageMeilleursScores) {
		this.affichageMeilleursScores = affichageMeilleursScores;
	}
	
	public int getApparitionAlien() {
		return apparitionAlien;
	}

	public void setApparitionAlien(int apparitionAlien) {
		this.apparitionAlien = apparitionAlien;
	}

	public int getVitesseAlien() {
		return vitesseAlien;
	}

	public void setVitesseAlien(int vitesseAlien) {
		this.vitesseAlien = vitesseAlien;
	}

	public int getScore() {
		return score;
	}
	
	public void setScore(int score1) {
		score = score1;
	}
	
	public int getNiveau() {
		return niveau;
	}
	
	public void setNiveau(int niveau) {
		this.niveau = niveau;
	}
	
	public int getTempoAlien() {
		return tempoAlien;
	}
	
	public void setTempoAlien(int tempoAlien) {
		this.tempoAlien = tempoAlien;
	}
	
	public int getVieJoueur() {
		return vieJoueur;
	}
	
	public void setVieJoueur(int vieJoueur1) {
		vieJoueur = vieJoueur1;
	}
	
	public int getVieAlien() {
		return vieAlien;
	}
	
	public void setVieAlien(int vieAlien) {
		this.vieAlien = vieAlien;
	}
	
	private void passerNiveau() {
		vitesseAlien = vitesseAlien + 1;
		apparitionAlien = apparitionAlien - 1;
	}
	
	// Methode permettant d'afficher le score
	protected void affichageScore(Graphics g) {
		g.setColor(Color.red);
		g.setFont(new Font("TimesRoman", Font.PLAIN, 20));
		g.drawString(String.valueOf("Score : "+score), 5, 15);
		g.drawString(String.valueOf("Vie : "+ vieJoueur), 5, 35);
		g.drawString(String.valueOf("Niveau : "+niveau), 5, 55);
	}
	
	protected void InitialisationJeu(Vaisseau Joueur) {
		score = 0;
		vieJoueur = 3;
		niveau = 1;
		apparitionAlien = 60;
		vitesseAlien = 40;
		vieAlien = 1; 
		Joueur.setX(376);
		
	}
	
}
