package SpaceInvaders;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Fenetre extends JFrame {

	// instance des calcul et d'affichage
	private MoteurJeu moteurJeu;
	private GameOver gameOver;

	public Fenetre() {
		// titre de la fen�tre
		super("Space Invader");
		//creation des instances
		this.moteurJeu=new MoteurJeu();
		// fermeture de l'application lorsque la fen�tre est ferm�e
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// pas de redimensionnement possible de la fen�tre
		setResizable(true);


		// creation d'un conteneur qui affichera le jeu
		final JPanel content = new JPanel() {
			@Override
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				// affichage du mod�le du jeu
				Fenetre.this.moteurJeu.affichage(g);
			}
		};

		// dimensions du conteneur
		content.setPreferredSize(new Dimension(800, 700));
		// arriere plan en blanc
		content.setBackground(Color.WHITE);
		//toujours en haut
		setAlwaysOnTop(true);
		// ajouter le conteneur � la fen�tre
		setContentPane(content);

		// le listener g�rant les entr�es au clavier
		content.addKeyListener(new KeyAdapter() {
		      @Override
		      public void keyPressed(KeyEvent e) {
		            Fenetre.this.moteurJeu.touchePresse(e);
		            Fenetre.this.moteurJeu.gestionDuClavier();
		      }
		      
		      public void keyReleased(KeyEvent e) {
		    	  Fenetre.this.moteurJeu.toucheRelache(e);
		    	  Fenetre.this.moteurJeu.gestionDuClavier();
		      }
		      		      
		});
		
		// s'assurer du focus pour le listener clavier
		setFocusable(false);
		content.setFocusable(true);



		//creation du thread infini
		

		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) { 
		
					// appel des calculs
					Fenetre.this.moteurJeu.calcul();
					// demander au thread de redessiner le conteneur
					content.repaint();
					// temporisation
					try {
						Thread.sleep(10);
					} catch (InterruptedException e) {
					}
				}
			}
		});

		//lancement du thread
		thread.start();

	}


	// Lancement du jeu 
	public static void main(String[] args) {
		// cr�ation de la fen�tre
		Fenetre fenetre = new Fenetre();
		// dimensionnement de la fen�re "au plus juste" suivant
		// la taille des composants qu'elle contient
		fenetre.pack();
		// centrage sur l'�cran
		fenetre.setLocationRelativeTo(null);
		// affichage
		fenetre.setVisible(true);
	}

} 