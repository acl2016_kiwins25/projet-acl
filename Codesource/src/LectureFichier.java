package SpaceInvaders;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

public class LectureFichier {

	private static BufferedWriter sortie_donnees;
	private static BufferedReader entree_donnees ;
	public static Vector<String> nomsJoueurs=new Vector<String>();
	public static Vector<Integer> scoresJoueurs=new Vector<Integer>();

	static void lectureLignes()  throws IOException{

		nomsJoueurs.clear();
		scoresJoueurs.clear();

		File Scores =  new File("scores.txt");
		entree_donnees =  new BufferedReader(new FileReader(Scores));
		String chaine ="";
		do {
			chaine =  entree_donnees.readLine();
			if (chaine !=  null) {
				String [] intermediaire=chaine.split(";");
				nomsJoueurs.add(intermediaire[0]);
				scoresJoueurs.add(Integer.parseInt(intermediaire[1]));

			}
		}  while (chaine !=  null);
		entree_donnees.close();
	}

	static void ecritureLignes()  throws IOException{


		File newScore =  new File("scores.txt");


		FileWriter donnees_a_ecrire = new FileWriter(newScore);
		sortie_donnees =  new BufferedWriter(donnees_a_ecrire);

		for(int i=0;i<nomsJoueurs.size();i++){

			String ligneAEcrire=nomsJoueurs.get(i)+";"+scoresJoueurs.get(i);

			sortie_donnees.write(ligneAEcrire);
			sortie_donnees.newLine();
		}



		sortie_donnees.close();
	}

	public static void compareScore(String nomJoueur, int scoreJoueur){

		int indiceScore=5;

		for(int i=scoresJoueurs.size()-1;i>=0;i--){
			if(scoresJoueurs.get(i)<=scoreJoueur)
			{
				indiceScore=i;
			}

		}

		if(indiceScore<5){
			scoresJoueurs.set(indiceScore, scoreJoueur);
			nomsJoueurs.set(indiceScore, nomJoueur);

		}

	}

}
