package SpaceInvaders;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Explosion extends ModeleJeu{
	
	private String image_explosion="explosion.png";
	private Image explosion;
	private int temporisation =0;
	
	protected int getTemporisation() {
		return temporisation;
	}

	protected void setTemporisation(int temporisation) {
		this.temporisation = temporisation;
	}

	private int x;
	private final int y;
	
	protected Explosion(int x, int y){
		try {
			this.explosion= ImageIO.read(new File(image_explosion));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.x=x;
		this.y=y;
	}
	
	protected void calcul(){
			
	}
	
	protected void affichage(Graphics g){
		
		JLabel vaisseau=new JLabel();
		g.drawImage(this.explosion, this.x, this.y, 60, 60, null);
		
	}
	
	
	

	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected void setX(int x) {
		this.x = x;
	}

	
	
	
	
}
