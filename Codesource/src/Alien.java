package SpaceInvaders;

import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JLabel;

public class Alien extends ModeleJeu{
	
	private String image_vaisseau_alien="alien.png";
	private Image vaisseauAlien;
	private int x,y;
	private int vieAlien;
	
	protected int getVieAlien() {
		return vieAlien;
	}


	protected void setVieAlien(int vieAlien) {
		this.vieAlien = vieAlien;
	}


	protected Alien(int vieAlien){
		try {
			this.vaisseauAlien= ImageIO.read(new File(image_vaisseau_alien));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.vieAlien = vieAlien;
		this.x=GenerationAlea();
		this.y=0;
	}
	
	
	protected void calcul(){
		deplacement();
		
	}

	
	protected void affichage(Graphics g){
		
		g.drawImage(this.vaisseauAlien, this.x, this.y, 40, 40, null);
		
		
	}
	
	protected int GenerationAlea(){
		int nbAleatoire;
		nbAleatoire = 10 + (int) (Math.random() * 750);
		return nbAleatoire;
	}
	
	
	private void deplacement(){
		
		this.y=this.y+20;		
	}
	
	protected boolean collisionShot(int xShot, int yShot){
		
		//taille de l'image du tir : 12x39
		
		if(xShot+12<this.x || xShot>(this.x+40) || yShot>(this.y+40) || yShot<this.y)
		{
			return false;
		}
		else
		{
			return true;
		}
		
	}

	protected int getY() {
		return y;
	}
	
	protected int getX() {
		return x;
	}
	
	
}
