package SpaceInvaders;


import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GameOver extends ModeleJeu{
	
	private String image_game_over="gameOver.png";
	private String image_game_over_2="gameOver2.png";
	private Image gameOver;
	private Image gameOver2;
	private int x = 140;
	private final int y = 150;
	
	protected GameOver(){
		try {
			this.gameOver= ImageIO.read(new File(image_game_over));
			this.gameOver2 = ImageIO.read(new File(image_game_over_2));
			} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	protected void calcul(){
			
	}
	
	protected void affichage(Graphics g){
		g.drawImage(this.gameOver2, 0, 0, 800, 700, null);
		g.drawImage(this.gameOver, this.x, this.y, 549, 245, null);
	}
	
	// Methode d'affichage du score et des consigne dans l'interface de game over
	protected void affichageGameOver(Graphics g, int tempoAffichage, int score){
		g.setColor(Color.WHITE);
		g.setFont(new Font("Monaco", Font.PLAIN, 20));
		g.drawString(String.valueOf("Votre score �tait de : "+score),315,420);
		if ((tempoAffichage<=50)&&(tempoAffichage>=25)) {
		g.drawString("Appuyez sur Entrer pour recommencer", 247, 500);
		}
	}
	

	protected int getX() {
		return x;
	}

	protected int getY() {
		return y;
	}

	protected void setX(int x) {
		this.x = x;
	}


	
	
	
	
}
