package SpaceInvaders;
import java.awt.Graphics;
import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Etoile {

	private String image_etoile="etoile.jpg";
	private Image etoile;
	private int x,y;
	
	protected Etoile(){
		try {
			this.etoile= ImageIO.read(new File(image_etoile));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		this.x=0;
		this.y=0;
	}
	
	protected void affichage(Graphics g){
		
		g.drawImage(this.etoile, this.x, this.y, 900, 700, null);
		
		
	}
}
